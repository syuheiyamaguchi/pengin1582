﻿using UnityEngine;
using System.Collections;

public class SelfMotion : MonoBehaviour {
	
	int Random_num;
	int Random_x;
	int Random_z;
	
	
	float RANDOM_NUM = 0.05f;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		Random_x = Random.Range(0, 2);
		
		Random_z = Random.Range(0, 2);	

/*		
ß		Random_x = new System.Random.Next(0,1);
		
		Random_z = new System.Random.Next(0,1);
*/		
		if( Random_x == 0 ){
			Random_x = -1;
		} else {
			Random_x = 1;
		}
		
		if( Random_z == 0 ){
			Random_z = -1;
		} else {
			Random_z = 1;
		}
		
		this.transform.position = new Vector3(this.transform.position.x + (Random_x * RANDOM_NUM),2.13f,this.transform.position.z + (Random_z * RANDOM_NUM));
		
	}
	
/*	public void FinishedPosition( object finished_position ){
		
		Debug.Log(finished_position);

		this.transform.position = finished_position.t;
//		this.transform.position = new Vector3 (finished_position.x,finished_position.y, finished_position.z);
	}
*/	
	public void SelfMotionStop( bool ret ){
				
		if(ret) {
			RANDOM_NUM = 0;
		} else {
			RANDOM_NUM = 0.05f;
		}
		
	}
}
