﻿using UnityEngine;
using System.Collections;

public class TimeControl : MonoBehaviour {

	bool finish_flag = false;
	float	 start_time	 = 0;
	
	//初期化処理．
	void Awake() {
//		GameObject time = GameObject.Find("Timer");
//		TextMesh tm = (TextMesh)time.GetComponent("TextMesh");
		
		start_time = Time.realtimeSinceStartup;
	}
	
	//終了管理.
	public void FinishFlagControl(bool flag){
		finish_flag = flag;
	}
	
	// Update is called once per frame
	void Update () {
		
		GameObject time = GameObject.Find("Timer");
		TextMesh tm = (TextMesh)time.GetComponent("TextMesh");

		
		if(finish_flag == false){
				
			tm.text = System.Convert.ToString(Time.realtimeSinceStartup - start_time);
			
		} else { 
			Debug.Log("true");
			tm.text = tm.text;
		}
	}
	
	
}
