﻿using UnityEngine;
using System.Collections;

public class TopControl : MonoBehaviour {
	
	private GameObject Button_9;
	private GameObject Button_12;
	
	//BGMを消えないオブジェクトに設定.
	public GameObject BGMSound = null;

	
	void Awake(){
		Button_9  = GameObject.Find("9Piece_Button");
		Button_12 = GameObject.Find("12Piece_Button");
	
		BGMSound  = GameObject.Find("BGMSound");
		
		//BGMオブジェクトがなければ生成.		
		if(BGMSound == null){
			BGMSound = (GameObject)Resources.Load("Prefab/BGMSound");
			Instantiate(BGMSound);

			BGMSound  = GameObject.Find("BGMSound(Clone)");
			BGMSound.name = "BGMSound";
		}
		
		DontDestroyOnLoad(BGMSound);
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log(PlayerPrefs.GetInt("mode"));
	}
	
	// マウスボタンが押されたとき.
	void 	OnMouseDown()
	{
		if(this.name == Button_9.name){
			Debug.Log ("Button_9");
			
			PlayerPrefs.SetInt("mode", 9);
			
		}else if (this.name == Button_12.name){
			Debug.Log ("Button_12");
			
			PlayerPrefs.SetInt("mode", 12);
			
		}
		
		Application.LoadLevel("main_pazlu");
	}
}
